package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class ReadHeadDuck implements Duck {

	@Override
	public String getInfo() {
		return "I'm Read head duck, quack quack";
	}
}
