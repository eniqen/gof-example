package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class FarmFactory implements AbstractFactory {

	@Override
	public Duck createDuck() {
		return new ReadHeadDuck();
	}

	@Override
	public Pig createPig() {
		return new DurocPig();
	}

	@Override
	public Rabbit createRabbit() {
		return new CaliforniaRabbit();
	}
}
