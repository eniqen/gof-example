package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class BugsBunny implements Rabbit {

	@Override
	public String getInfo() {
		return "I'm  bugs bunny. What's up dog";
	}
}
