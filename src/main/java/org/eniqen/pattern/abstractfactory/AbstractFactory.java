package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface AbstractFactory {

	Duck createDuck();

	Pig createPig();

	Rabbit createRabbit();
}
