package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class CartoonFactory implements AbstractFactory {

	@Override
	public Duck createDuck() {
		return new DuffyDuck();
	}

	@Override
	public Pig createPig() {
		return new Porky();
	}

	@Override
	public Rabbit createRabbit() {
		return new BugsBunny();
	}
}
