package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class DuffyDuck implements Duck {

	@Override
	public String getInfo() {
		return "I'm Duffy. Hello";
	}
}
