package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class CaliforniaRabbit implements Rabbit {

	@Override
	public String getInfo() {
		return "I'm California rabbit. Munch munch";
	}
}
