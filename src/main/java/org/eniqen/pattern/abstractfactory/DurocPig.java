package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class DurocPig implements Pig {

	@Override
	public String getInfo() {
		return "I'm Duroc pig. Oink oink";
	}
}
