package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {

	private Duck duck;
	private Pig pig;
	private Rabbit rabbit;

	private void createFarm(final AbstractFactory factory) {
		setDuck(factory.createDuck());
		setPig(factory.createPig());
		setRabbit(factory.createRabbit());
	}

	public void setDuck(Duck duck) {
		this.duck = duck;
	}

	public void setPig(Pig pig) {
		this.pig = pig;
	}

	public void setRabbit(Rabbit rabbit) {
		this.rabbit = rabbit;
	}

	public Duck getDuck() {
		return duck;
	}

	public Pig getPig() {
		return pig;
	}

	public Rabbit getRabbit() {
		return rabbit;
	}

	public static void main(String[] args) {

		final App app = new App();

		System.out.println("Create Farm Factory");
		app.createFarm(new FarmFactory());
		System.out.println(app.getDuck().getInfo());
		System.out.println(app.getRabbit().getInfo());
		System.out.println(app.getPig().getInfo());

		System.out.println("========================");

		System.out.println("Create Cartoon Factory");
		app.createFarm(new CartoonFactory());
		System.out.println(app.getDuck().getInfo());
		System.out.println(app.getRabbit().getInfo());
		System.out.println(app.getPig().getInfo());
	}
}
