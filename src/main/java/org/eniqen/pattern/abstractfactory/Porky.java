package org.eniqen.pattern.abstractfactory;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Porky implements Pig {
	@Override
	public String getInfo() {
		return "I'm Porky. Thats all folks";
	}
}
