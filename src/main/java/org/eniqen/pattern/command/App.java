package org.eniqen.pattern.command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {
		final Hunter hunter = new Hunter();
		final Target target = new Duck();

		hunter.executeCommand(new CatchCommand(), target);
		hunter.executeCommand(new KillCommand(), target);

		hunter.undoCommand();
		hunter.undoCommand();
		hunter.redoCommand();
	}
}
