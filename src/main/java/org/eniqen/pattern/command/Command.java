package org.eniqen.pattern.command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Command {

	void execute(final Target target);

	void undo();

	void redo();
}
