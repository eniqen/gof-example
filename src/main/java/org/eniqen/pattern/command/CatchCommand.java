package org.eniqen.pattern.command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class CatchCommand implements Command {

	private Target target;

	@Override
	public void execute(Target target) {
		target.setCatched(Catched.CATCH);
		this.target = target;
	}

	@Override
	public void undo() {
		if (target != null) {
			target.setCatched(Catched.FREE);
		}
	}

	@Override
	public void redo() {
		if (target != null) {
			target.setCatched(Catched.CATCH);
		}
	}
}
