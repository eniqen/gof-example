package org.eniqen.pattern.command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public abstract class Target {

	private Status status;
	private Catched catched;

	public Target() {
		setStatus(Status.ALIVE);
		setCatched(Catched.FREE);
	}

	abstract String getName();

	public void setStatus(Status status) {
		this.status = status;
	}

	public void setCatched(Catched catched) {
		this.catched = catched;
	}
}
