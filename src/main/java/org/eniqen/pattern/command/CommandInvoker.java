package org.eniqen.pattern.command;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class CommandInvoker {

	private List<Target> objects;

	public CommandInvoker(final List<? extends Target> objects) {
		this.objects = Collections.unmodifiableList(objects);
	}

	public void applyCommand(final Command command) {
		if (command != null) {
			objects.forEach(command::execute);
		}
	}

	public void applyCommand(final Command command, final Command... commands) {
		applyCommand(command);
		if (commands != null) {
			Stream.of(commands).forEach(this::applyCommand);
		}
	}
}
