package org.eniqen.pattern.command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class KillCommand implements Command {

	private Target target;

	@Override
	public void execute(Target target) {
		target.setStatus(Status.DEAD);
		this.target = target;
	}

	@Override
	public void undo() {
		if (target != null) {
			target.setStatus(Status.ALIVE);
		}
	}

	@Override
	public void redo() {
		if (target != null) {
			target.setStatus(Status.ALIVE);
		}
	}
}
