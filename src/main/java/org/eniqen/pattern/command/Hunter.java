package org.eniqen.pattern.command;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Hunter {

	private Deque<Command> undoCommand = new LinkedList<>();
	private Deque<Command> redoCommand = new LinkedList<>();

	public void executeCommand(final Command command, final Target target) {
		command.execute(target);
		undoCommand.offerLast(command);
	}

	public void undoCommand() {
		if (!undoCommand.isEmpty()) {
			final Command previousCommand = undoCommand.pollLast();
			this.redoCommand.offerFirst(previousCommand);
			previousCommand.undo();
		}
	}

	public void redoCommand() {
		if (!redoCommand.isEmpty()) {
			final Command previousCommand = this.redoCommand.pollLast();
			undoCommand.offerLast(previousCommand);
			previousCommand.redo();
		}
	}
}
