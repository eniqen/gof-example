package org.eniqen.pattern.command;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Duck extends Target {

	@Override
	public String getName() {
		return "Duck";
	}
}
