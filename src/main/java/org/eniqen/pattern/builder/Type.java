package org.eniqen.pattern.builder;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public enum Type {
	MALLARD, RUBBER
}
