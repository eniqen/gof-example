package org.eniqen.pattern.builder;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public enum Color {
	RED, GREEN, ORANGE
}
