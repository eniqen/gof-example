package org.eniqen.pattern.builder;

import java.util.Objects;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Duck {

	private String name;
	private Type type;
	private Color color;
	private int weight;

	private Duck(final DuckBuilder duckBuilder) {
		this.name = duckBuilder.name;
		this.type = duckBuilder.type;
		this.color = duckBuilder.color;
		this.weight = duckBuilder.weight;
	}

	@Override
	public String toString() {
		return "Duck{" +
			   "name='" + name + '\'' +
			   ", type=" + type +
			   ", color=" + color +
			   ", weight=" + weight +
			   '}';
	}

	static class DuckBuilder {

		private final String name;
		private final Type type;
		private Color color;
		private int weight;

		public DuckBuilder(final String name, final Type type) {
			Objects.requireNonNull(name, () -> "name can't be null");
			Objects.requireNonNull(type, () -> "type can't be null");
			this.name = name;
			this.type = type;
		}

		public DuckBuilder withColor(final Color color) {
			this.color = color;
			return this;
		}

		public DuckBuilder withWeight(final int weight) {
			this.weight = weight;
			return this;
		}

		public Duck build() {
			return new Duck(this);
		}
	}
}
