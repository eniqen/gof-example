package org.eniqen.pattern.builder;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {
		final Duck duck = new Duck.DuckBuilder("O_o", Type.MALLARD)
								  .withColor(Color.GREEN)
								  .withWeight(5)
								  .build();

		System.out.println(duck.toString());
	}
}
