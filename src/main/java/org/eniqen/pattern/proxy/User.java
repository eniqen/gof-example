package org.eniqen.pattern.proxy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class User {

	private final String login;
	private final String permission;

	private User(final String login, final String permission) {
		this.login = login;
		this.permission = permission;
	}

	public static User of(final String login, final String permission){
		return new User(login, permission);
	}

	public String getLogin() {
		return login;
	}

	public String getPermission() {
		return permission;
	}
}
