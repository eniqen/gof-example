package org.eniqen.pattern.proxy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {
		final User incorrectCredential = User.of("login", "permission");
		final Folderable folder = new FolderProxy(incorrectCredential);
		folder.open();

		System.out.println();
		final User correctCredential = User.of("vasya", "administrator");
		final Folderable folder2 = new FolderProxy(correctCredential);
		folder2.open();
	}
}
