package org.eniqen.pattern.proxy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Folderable {

	void open();
}
