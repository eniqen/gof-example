package org.eniqen.pattern.proxy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Folder implements Folderable {

	@Override
	public void open() {
		System.out.println("Open folder");
	}
}
