package org.eniqen.pattern.proxy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class FolderProxy implements Folderable {

	private Folder folder;
	private User user;

	public FolderProxy(final User user) {
		this.user = user;
	}

	@Override
	public void open() {
		if (allowed()) {
			System.out.println("Permission allowed");
			this.folder = new Folder();
			folder.open();
		} else {
			System.out.println("Permission denied");
		}
	}

	private boolean allowed(){
		return "administrator".equalsIgnoreCase(user.getPermission())
			   && "vasya".equalsIgnoreCase(user.getLogin());
	}
}
