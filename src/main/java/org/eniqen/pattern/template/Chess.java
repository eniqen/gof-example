package org.eniqen.pattern.template;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Chess extends Game {
	@Override
	public void createUnit() {
		System.out.println("Create chess unit");
	}

	@Override
	public void createBoard() {
		System.out.println("Create chess board");
	}
}
