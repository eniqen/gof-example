package org.eniqen.pattern.template;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {

		final Game game = new Chess();
		game.play();

		new GameJava8<String>().template("Template Method With Java 8",
										 message -> System.out.println(message.toUpperCase()),
										 message -> System.out.println(message.toLowerCase()));
	}
}
