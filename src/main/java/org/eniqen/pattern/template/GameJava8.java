package org.eniqen.pattern.template;

import java.util.function.Consumer;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class GameJava8<T> {

	public final void template(T text, Consumer<T> consumer, Consumer<T> consumer2) {
		consumer.accept(text);
		consumer2.accept(text);
	}
}
