package org.eniqen.pattern.template;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Monopoly extends Game {
	@Override
	public void createUnit() {
		System.out.println("Prepate figure for game");
	}

	@Override
	public void createBoard() {
		System.out.println("create monopoly board");
	}
}
