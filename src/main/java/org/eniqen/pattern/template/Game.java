package org.eniqen.pattern.template;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public  abstract class Game {

	public void play() {
		createUnit();
		createBoard();
	}

	protected abstract void createUnit();
	protected abstract void createBoard();
}
