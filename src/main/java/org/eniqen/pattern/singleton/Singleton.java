package org.eniqen.pattern.singleton;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public final class Singleton {

	private Singleton() {
	}

	static Singleton getInstance(){
		return Holder.INSTANCE;
	}

	private static final class Holder {

		private Holder(){}

		private static final Singleton INSTANCE = new Singleton();
	}
}
