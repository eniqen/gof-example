package org.eniqen.pattern.singleton;

import java.util.Objects;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {

		final Singleton instance = Singleton.getInstance();
		final Singleton instance2 = Singleton.getInstance();

		System.out.println(Objects.equals(instance, instance2));
	}
}
