package org.eniqen.pattern.adapter;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Goose {

	public void display(){
		System.out.println(" Hello i'm goose quack quack");
	}
}
