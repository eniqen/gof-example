package org.eniqen.pattern.adapter;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Duck {

	void quack();
}
