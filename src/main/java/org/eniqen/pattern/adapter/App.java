package org.eniqen.pattern.adapter;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {

		final Goose goose = new Goose();
		final Duck duck = new GooseToDuckAdapter();

		duck.quack();
	}
}
