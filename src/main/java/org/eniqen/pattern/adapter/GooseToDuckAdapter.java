package org.eniqen.pattern.adapter;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class GooseToDuckAdapter implements Duck {
	private Goose goose;

	public GooseToDuckAdapter() {
		this.goose = new Goose();
	}

	@Override
	public void quack() {
		goose.display();
	}
}
