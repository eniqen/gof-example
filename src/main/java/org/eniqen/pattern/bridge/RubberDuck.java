package org.eniqen.pattern.bridge;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class RubberDuck implements Duck {
	@Override
	public void display() {
		System.out.println("Rubber duck can't quack");
	}
}
