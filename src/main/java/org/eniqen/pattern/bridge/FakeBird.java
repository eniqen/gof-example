package org.eniqen.pattern.bridge;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class FakeBird extends Bird {

	public FakeBird(Duck duck) {
		super(duck);
	}

	@Override
	public void quack() {
		this.duck.display();
	}
}
