package org.eniqen.pattern.bridge;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Duck {
	void display();
}
