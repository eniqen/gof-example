package org.eniqen.pattern.bridge;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {
		final Duck duck = new RubberDuck();
		final Bird bird = new FakeBird(duck);

		bird.quack();
	}
}
