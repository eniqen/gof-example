package org.eniqen.pattern.bridge;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public abstract class Bird {

	protected Duck duck;

	public Bird(Duck duck) {
		this.duck = duck;
	}

	public abstract void quack();
}
