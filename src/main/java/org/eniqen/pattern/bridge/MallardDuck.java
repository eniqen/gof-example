package org.eniqen.pattern.bridge;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class MallardDuck implements Duck {
	@Override
	public void display() {
		System.out.println("Mallard quack");
	}
}
