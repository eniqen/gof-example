package org.eniqen.pattern.flyweight;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public enum DuckType {
	MALLARD, RUBBER
}
