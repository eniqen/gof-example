package org.eniqen.pattern.flyweight;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class DuckFactory {

	private final Map<DuckType, Duck> ducks = new EnumMap<>(DuckType.class);

	public Duck getByType(DuckType duckType) {
		Duck duck = this.ducks.get(duckType);
		if (duck == null) {
			switch (duckType) {
				case MALLARD:
					duck = new MallardDuck();
					ducks.put(duckType, duck);
					break;
				case RUBBER:
					duck =  new RubberDuck();
					ducks.put(duckType, duck);
					break;
				default:
					break;
			}
		}
		return duck;
	}
}
