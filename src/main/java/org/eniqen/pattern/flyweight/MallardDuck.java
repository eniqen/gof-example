package org.eniqen.pattern.flyweight;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class MallardDuck implements Duck {
	@Override
	public void info() {
		System.out.println("Mallard duck");
	}
}
