package org.eniqen.pattern.flyweight;

import java.util.ArrayList;
import java.util.List;

import static org.eniqen.pattern.flyweight.DuckType.*;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {

		final DuckFactory duckFactory = new DuckFactory();

		final List<Duck> ducks = new ArrayList<>();

		for (int index = 0; index < 10; index++) {
			if (index % 2 == 0) {
				ducks.add(duckFactory.getByType(MALLARD));
			} else {
				ducks.add(duckFactory.getByType(RUBBER));
			}
		}
		ducks.forEach(Duck::info);
	}
}
