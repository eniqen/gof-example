package org.eniqen.pattern.flyweight;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class RubberDuck implements Duck {
	@Override
	public void info() {
		System.out.println("Rubber duck");
	}
}
