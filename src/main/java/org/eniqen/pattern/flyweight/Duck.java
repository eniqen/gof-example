package org.eniqen.pattern.flyweight;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Duck {
	void info();
}
