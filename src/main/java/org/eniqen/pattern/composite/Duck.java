package org.eniqen.pattern.composite;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Duck {

	void display();
}
