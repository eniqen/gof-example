package org.eniqen.pattern.composite;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {
		final CompositeDuck compositeDuck = new CompositeDuck("ROot");
		Duck duck1 = new Duckling("DUCK-1");
		Duck duck2 = new Duckling("DUCK-2");
		CompositeDuck duck3 = new CompositeDuck("DUCK-3");
		Duck duck4 = new Duckling("DUCK-4");
		Duck duck5 = new Duckling("DUCK-5");
		CompositeDuck duck6 = new CompositeDuck("DUCK-6");
		Duck duck7 = new Duckling("DUCK-7");
		Duck duck8 = new Duckling("DUCK-8");

		compositeDuck.add(duck1);
		compositeDuck.add(duck2);
		compositeDuck.add(duck3);

		duck3.add(duck4);
		duck3.add(duck5);
		duck3.add(duck6);

		duck6.add(duck7);
		duck6.add(duck8);

		compositeDuck.display();

	}
}
