package org.eniqen.pattern.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class CompositeDuck implements Duck {

	private List<Duck> child = new ArrayList<>();
	private String name;

	public CompositeDuck(String name) {
		this.name = name;
	}

	@Override
	public void display() {
		System.out.println(name);
		this.child.forEach(Duck::display);
	}

	public void add(Duck duck){
		this.child.add(duck);
	}
	public void remove(Duck duck){
		this.child.remove(duck);
	}
}
