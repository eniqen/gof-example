package org.eniqen.pattern.composite;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class Duckling implements Duck {
	private String name;

	public Duckling(String name) {
		this.name = name;
	}

	@Override
	public void display() {
		System.out.println(name);
	}
}
