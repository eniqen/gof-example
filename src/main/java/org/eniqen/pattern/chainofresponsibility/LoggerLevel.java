package org.eniqen.pattern.chainofresponsibility;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public enum LoggerLevel {
	INFO, WARN, ERROR
}
