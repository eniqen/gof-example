package org.eniqen.pattern.chainofresponsibility;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class ErrorLoggerHandler extends LoggerHandler {

	public ErrorLoggerHandler(LoggerLevel level) {
		super(level);
	}

	@Override
	protected void write(String message) {
		System.out.println("ERROR " + message);
	}
}
