package org.eniqen.pattern.chainofresponsibility;

import static org.eniqen.pattern.chainofresponsibility.LoggerLevel.*;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {

	public static void main(String[] args) {
		final LoggerHandler logger = new InfoLoggerHandler(INFO);
		final LoggerHandler warnLoggerHandlerHandler = new WarnLoggerHandler(WARN);
		final LoggerHandler errorLoggerHandler = new ErrorLoggerHandler(ERROR);

		logger.setNext(warnLoggerHandlerHandler);
		warnLoggerHandlerHandler.setNext(errorLoggerHandler);

		logger.handle(new LoggerInfo(INFO, "info"));
		System.out.println();

		logger.handle(new LoggerInfo(WARN, "info"));
		System.out.println();

		logger.handle(new LoggerInfo(ERROR, "info"));
	}
}
