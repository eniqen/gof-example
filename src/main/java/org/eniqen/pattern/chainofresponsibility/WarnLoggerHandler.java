package org.eniqen.pattern.chainofresponsibility;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class WarnLoggerHandler extends LoggerHandler {

	public WarnLoggerHandler(LoggerLevel level) {
		super(level);
	}

	@Override
	protected void write(String message) {
		System.out.println("WARN " + message);
	}
}
