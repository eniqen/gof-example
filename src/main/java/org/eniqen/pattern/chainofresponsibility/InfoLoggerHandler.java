package org.eniqen.pattern.chainofresponsibility;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class InfoLoggerHandler extends LoggerHandler {

	public InfoLoggerHandler(LoggerLevel level) {
		super(level);
	}

	@Override
	protected void write(String message) {
		System.out.println("INFO " + message);
	}
}
