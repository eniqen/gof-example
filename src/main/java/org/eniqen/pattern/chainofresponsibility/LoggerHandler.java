package org.eniqen.pattern.chainofresponsibility;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public abstract class LoggerHandler {

	private LoggerHandler next;
	private final LoggerLevel level;

	public LoggerHandler(final LoggerLevel level) {
		this.level = level;
	}

	public void handle(LoggerInfo loggerInfo) {
		if(level.ordinal() <= loggerInfo.getLevel().ordinal()){
			write(loggerInfo.getMessage());
		}

		if (next != null) {
			next.handle(loggerInfo);
		}
	}

	protected abstract void write(String message);

	public void setNext(LoggerHandler next) {
		this.next = next;
	}
}
