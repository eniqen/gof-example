package org.eniqen.pattern.chainofresponsibility;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class LoggerInfo {

	private final LoggerLevel level;
	private final String message;

	public LoggerInfo(LoggerLevel level, String message) {
		this.level = level;
		this.message = message;
	}

	public LoggerLevel getLevel() {
		return level;
	}

	public String getMessage() {
		return message;
	}
}
