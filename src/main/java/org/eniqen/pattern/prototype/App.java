package org.eniqen.pattern.prototype;

import java.util.Objects;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) throws CloneNotSupportedException {
		final Prototype duckPrototype = new DuckPrototype("DuffyDuck", 50);
		final Prototype clone = duckPrototype.clone();
		System.out.println(Objects.equals(duckPrototype, clone));
	}
}
