package org.eniqen.pattern.prototype;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class DuckPrototype implements Prototype {

	private String name;
	private Integer weight;

	public DuckPrototype() {
	}

	public DuckPrototype(String name, Integer weight) {
		this.name = name;
		this.weight = weight;
	}

	@Override
	public Prototype clone() throws CloneNotSupportedException {
		return (Prototype) super.clone();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}
}
