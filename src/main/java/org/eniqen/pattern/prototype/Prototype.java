package org.eniqen.pattern.prototype;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Prototype extends Cloneable {
	Prototype clone() throws CloneNotSupportedException;
}
