package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class RubberDuck extends Duck {

	public RubberDuck(FlyStrategy flyStrategy, QuackStrategy quackStrategy) {
		super(flyStrategy, quackStrategy);
	}

	@Override
	public void display() {
		System.out.println("I'm RubberDuck");
	}

	public RubberDuck() {
		super(new FlyNoWayStrategy(), new MuteQuackStrategy());
	}
}
