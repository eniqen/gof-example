package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
@FunctionalInterface
public interface QuackStrategy {
	void quack();
}
