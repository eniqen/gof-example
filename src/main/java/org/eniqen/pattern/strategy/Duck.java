package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public abstract class Duck {
	private FlyStrategy flyStrategy;
	private QuackStrategy quackStrategy;

	public Duck(FlyStrategy flyStrategy, QuackStrategy quackStrategy) {
		this.flyStrategy = flyStrategy;
		this.quackStrategy = quackStrategy;
	}

	public abstract void display();

	public void performFly(){
		flyStrategy.fly();
	}

	public void performQuack(){
		quackStrategy.quack();
	}

	public void setFlyStrategy(FlyStrategy flyStrategy) {
		this.flyStrategy = flyStrategy;
	}

	public void setQuackStrategy(QuackStrategy quackStrategy) {
		this.quackStrategy = quackStrategy;
	}
}
