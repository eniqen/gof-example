package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {
		final Duck duck1 = new MallardDuck();
		final Duck duck2 = new RubberDuck();

		duck1.display();
		duck1.performFly();
		duck1.performQuack();

		System.out.println();

		duck2.display();
		duck2.performFly();
		duck2.performQuack();
		duck2.setFlyStrategy(new FlyWithWingsStrategy());
		duck2.setQuackStrategy(new SqueakStrategy());
		duck2.performFly();
		duck2.performQuack();

		//Java 8
		duck2.setFlyStrategy(() -> System.out.printf("Java 8 fly strategy"));
		duck2.performFly();
	}
}
