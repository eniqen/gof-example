package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class FlyNoWayStrategy implements FlyStrategy {

	@Override
	public void fly() {
		System.out.println("I can't fly");
	}
}
