package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class MuteQuackStrategy implements QuackStrategy {
	@Override
	public void quack() {
		System.out.println(" O_o ");
	}
}
