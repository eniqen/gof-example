package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class MallardDuck extends Duck {

	public MallardDuck(FlyStrategy flyStrategy, QuackStrategy quackStrategy) {
		super(flyStrategy, quackStrategy);
	}

	@Override
	public void display() {
		System.out.println("I'm MallardDuck");
	}

	public MallardDuck() {
		super(new FlyWithWingsStrategy(), new SqueakStrategy());
	}
}
