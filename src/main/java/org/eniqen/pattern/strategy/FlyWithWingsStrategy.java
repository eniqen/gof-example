package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class FlyWithWingsStrategy implements FlyStrategy {

	@Override
	public void fly() {
		System.out.println("I fly with wings");
	}
}
