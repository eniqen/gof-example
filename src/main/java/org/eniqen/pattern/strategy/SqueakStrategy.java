package org.eniqen.pattern.strategy;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class SqueakStrategy implements QuackStrategy {
	@Override
	public void quack() {
		System.out.println("Quack quack");
	}
}
