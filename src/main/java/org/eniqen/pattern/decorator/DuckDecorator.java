package org.eniqen.pattern.decorator;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public abstract class DuckDecorator implements Duck {

	private Duck duck;

	public DuckDecorator(Duck duck) {
		this.duck = duck;
	}

	public Duck getDuck() {
		return duck;
	}
}
