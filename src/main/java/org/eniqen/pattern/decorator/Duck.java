package org.eniqen.pattern.decorator;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Duck {
	String info();
}
