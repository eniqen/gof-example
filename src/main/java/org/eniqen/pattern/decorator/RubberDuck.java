package org.eniqen.pattern.decorator;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class RubberDuck implements Duck {

	@Override
	public String info() {
		return "Rubber duck";
	}
}
