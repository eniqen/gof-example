package org.eniqen.pattern.decorator;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class QuackDuckDecorator extends DuckDecorator {

	public QuackDuckDecorator(Duck duck) {
		super(duck);
	}

	@Override
	public String info() {
		return super.getDuck().info() + " Can quack with quack decorator ";
	}
}
