package org.eniqen.pattern.decorator;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class FlyDuckDecorator extends DuckDecorator {

	public FlyDuckDecorator(Duck duck) {
		super(duck);
	}

	@Override
	public String info() {
		return super.getDuck().info() + " Can fly with fly decorator";
	}
}
