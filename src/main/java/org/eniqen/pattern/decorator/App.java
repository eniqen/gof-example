package org.eniqen.pattern.decorator;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {

		final Duck duck = new RubberDuck();
		System.out.println("1 - " + duck.info());

		final Duck flyDuck = new FlyDuckDecorator(duck);
		System.out.println("2 - " + flyDuck.info());

		final Duck quackableDuck = new QuackDuckDecorator(duck);
		System.out.println("3 - " + quackableDuck.info());

		final Duck superRubberDuck = new FlyDuckDecorator(new QuackDuckDecorator(new RubberDuck()));
		System.out.println("4 - " + superRubberDuck.info());
	}
}
