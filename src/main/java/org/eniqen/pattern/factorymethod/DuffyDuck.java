package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class DuffyDuck implements Duck {
	@Override
	public void quack() {
		System.out.println("Hello i'm Duffy duck");
	}
}
