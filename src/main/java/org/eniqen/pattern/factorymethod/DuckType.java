package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public enum DuckType {
	CARTOON, RUBBER, MALLARD
}
