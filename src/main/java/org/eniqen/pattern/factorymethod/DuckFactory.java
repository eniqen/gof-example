package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class DuckFactory implements Factory {
	@Override
	public Duck create(DuckType type) {
		switch (type) {
			case CARTOON:
				return new DuffyDuck();
			case MALLARD:
				return new MallardDuck();
			case RUBBER:
				return new RubberDuck();
			default:
				return new StubDuck();
		}
	}
}
