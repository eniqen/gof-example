package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class MallardDuck implements Duck {
	@Override
	public void quack() {
		System.out.println("QUACK QUACK");
	}
}
