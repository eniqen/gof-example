package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class RubberDuck implements Duck {
	@Override
	public void quack() {
		System.out.println("I'm rubber duck");
	}
}
