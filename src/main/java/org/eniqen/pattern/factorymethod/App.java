package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {
		final Factory factory = new DuckFactory();
		final Duck duck = factory.create(DuckType.CARTOON);
		duck.quack();
	}
}
