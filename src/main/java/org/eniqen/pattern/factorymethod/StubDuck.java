package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class StubDuck implements Duck {
	@Override
	public void quack() {
	}
}
