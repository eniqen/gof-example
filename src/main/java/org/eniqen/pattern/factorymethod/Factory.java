package org.eniqen.pattern.factorymethod;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public interface Factory {

	Duck create(DuckType duckType);
}
