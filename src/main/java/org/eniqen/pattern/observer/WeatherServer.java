package org.eniqen.pattern.observer;

import java.util.Observable;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class WeatherServer extends Observable {

	private int info;

	public WeatherServer(int info) {
		this.info = info;
	}

	public void setX(int info) {
		if (this.info != info) {
			setChanged();
			notifyObservers(info);
		}
		this.info = info;
	}
}
