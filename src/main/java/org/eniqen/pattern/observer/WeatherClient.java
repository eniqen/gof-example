package org.eniqen.pattern.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class WeatherClient implements Observer {

	public WeatherClient(final Observable server) {
		server.addObserver(this);
	}

	@Override
	public void update(Observable server, Object arg) {
		if (server instanceof WeatherServer) {
			System.out.println("Get info from server = " + arg + " Client id = " + this.toString());
		}
	}
}
