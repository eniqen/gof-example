package org.eniqen.pattern.observer;

import java.util.Observer;

/**
 * @author Mikhail Nemenko {@literal <nemenkoma@gmail.com>}
 */
public class App {
	public static void main(String[] args) {

		final WeatherServer server = new WeatherServer(5);
		final Observer observer = new WeatherClient(server);
		final Observer observer2 = new WeatherClient(server);

		server.setX(2);
		System.out.println("============");
		server.setX(5);
	}
}
